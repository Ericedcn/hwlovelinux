## 10 nov 2020

La comanda lsblk és per extreure informació de les particions, sistemes de fitxers i punts de muntatge:

```
[professor@localhost ~]$ lsblk 
NAME   MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0    7:0    0  97.1M  1 loop /var/lib/snapd/snap/core/9993
loop1    7:1    0  17.9M  1 loop /var/lib/snapd/snap/pdftk/9
sda      8:0    0 111.8G  0 disk 
├─sda1   8:1    0   105G  0 part /
└─sda2   8:2    0     5G  0 part [SWAP]
sdb      8:16   0 465.8G  0 disk 
├─sdb1   8:17   0     1K  0 part 
└─sdb5   8:21   0 465.8G  0 part 
sr0     11:0    1  1024M  0 rom  
```

Alias per crear una comanda que mostri columnes interessants d'un disk que m'ofereix la comanda lsblk:

```
[root@localhost ~]# alias lsdisk='lsblk -o NAME,MODEL,FSTYPE,SIZE,TYPE,MOUNTPOINT'
[root@localhost ~]# lsdisk 
NAME   MODEL                     FSTYPE     SIZE TYPE MOUNTPOINT
loop0                            squashfs  97.1M loop /var/lib/snapd/snap/core/9993
loop1                            squashfs  17.9M loop /var/lib/snapd/snap/pdftk/9
sda    Samsung_SSD_850_EVO_120GB          111.8G disk 
├─sda1                           ext4       105G part /
└─sda2                           swap         5G part [SWAP]
sdb    TOSHIBA_DT01ACA050                 465.8G disk 
├─sdb1                                        1K part 
└─sdb5                           ext4     465.8G part 
sr0    HL-DT-ST_DVDRAM_GH24NSC0            1024M rom  
````


