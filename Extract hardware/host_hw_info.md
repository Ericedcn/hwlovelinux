# system 
Equipo aula **f2A**
17/11/2020


# system model, bios version and bios date, how old is the hardware?
For system model and to know how old is the hardware is use BIOS with version and date.
```
[root@a20 ~]# dmidecode -s bios-version
F5
[root@a20 ~]# dmidecode -s bios-release-date
01/17/2014
```


# mainboard model, link to manual, link to product

memory banks (free or occupied)
X570 AORUS ELITE
#https://www.google.com/search?q=memory+banks+of+x570+aorus+elite&tbm=isch&ved=2ahUKEwiJ-72tj4ntAhWO0uAKHU3AD3sQ2-cCegQIABAA&oq=memory+banks+of+x570&gs_lcp=CgNpbWcQARgAMgQIIxAnOgQIABAeOgYIABAFEB5QmFRY82pgk3loAHAAeACAAVqIAZ0FkgEBOJgBAKABAaoBC2d3cy13aXotaW1nwAEB&sclient=img&ei=UISzX4mTB46lgwfNgL_YBw&bih=799&biw=1440&client=firefox-b-d#imgrc=zRAvkjCmByDUBM

how many disks and types can be connected?
the x570 aorus elite have 4 disk and type can be connected.

chipset, link to
#https://hardzone.es/2019/05/21/diagrama-chipset-amd-x570-pcie-40/


# cpu

cpu model, year, cores, threads, cache

lscpu is a small and quick command that does not need any options. It would simply print the cpu hardware details in a user-friendly format.
```
[root@a20 ~]# lscpu
Architecture:                    x86_64
CPU op-mode(s):                  32-bit, 64-bit
Byte Order:                      Little Endian
Address sizes:                   39 bits physical, 48 bits virtual
CPU(s):                          2
On-line CPU(s) list:             0,1
Thread(s) per core:              1
Core(s) per socket:              2
Socket(s):                       1
NUMA node(s):                    1
Vendor ID:                       GenuineIntel
CPU family:                      6
Model:                           60
Model name:                      Intel(R) Pentium(R) CPU G3250 @ 3.20GHz
Stepping:                        3
CPU MHz:                         1676.155
CPU max MHz:                     3200.0000
CPU min MHz:                     800.0000
BogoMIPS:                        6385.51
Virtualization:                  VT-x
L1d cache:                       64 KiB
L1i cache:                       64 KiB
L2 cache:                        512 KiB
L3 cache:                        3 MiB
NUMA node0 CPU(s):               0,1

```
socket
```

```

# pci

number of pci slots, lanes available
```
[root@a20 ~]# dmidecode -t slot
# dmidecode 3.2
Getting SMBIOS data from sysfs.
SMBIOS 2.7 present.

Handle 0x0020, DMI type 9, 17 bytes
System Slot Information
	Designation: J6B2
	Type: x16 PCI Express
	Current Usage: In Use
	Length: Long
	ID: 0
	Characteristics:
		3.3 V is provided
		Opening is shared
		PME signal is supported
	Bus Address: 0000:00:02.0
```

devices connected

``` 
[ism5230318@LAPTOP-S3C1BNSQ ~]$ lsblk
NAME        MAJ:MIN RM   SIZE RO TYPE MOUNTPOINT
loop0         7:0    0  62.1M  1 loop /var/lib/snapd/snap/gtk-common-themes/1506
loop1         7:1    0 162.9M  1 loop /var/lib/snapd/snap/gnome-3-28-1804/145
loop2         7:2    0    31M  1 loop /var/lib/snapd/snap/snapd/9721
loop3         7:3    0 260.7M  1 loop /var/lib/snapd/snap/kde-frameworks-5-core1
loop4         7:4    0  55.3M  1 loop /var/lib/snapd/snap/core18/1885
loop5         7:5    0   251M  1 loop /var/lib/snapd/snap/gimp/297
nvme0n1     259:0    0 238.5G  0 disk 
├─nvme0n1p1 259:1    0   260M  0 part /boot/efi
├─nvme0n1p2 259:2    0    16M  0 part 
├─nvme0n1p3 259:3    0 168.9G  0 part 
├─nvme0n1p4 259:4    0   980M  0 part 
├─nvme0n1p5 259:5    0  49.8G  0 part /
└─nvme0n1p6 259:6    0   7.4G  0 part [SWAP]

```

network device, model, kernel module, speed

audio device, model, kernel module

vga device, model, kernel module

# hardisks

/dev/* , model, bus type, bus speed

To view all partitions of specific hard disk use the option ‘-l‘ with device name.
```
[root@a20 ~]# fdisk -l
Disk /dev/sda: 465.78 GiB, 500107862016 bytes, 976773168 sectors
Disk model: ST500DM002-1BD14
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 4096 bytes
I/O size (minimum/optimal): 4096 bytes / 4096 bytes
Disklabel type: dos
Disk identifier: 0x4a15410d

Device     Boot     Start       End   Sectors   Size Id Type
/dev/sda1            2048 976773119 976771072 465.8G  5 Extended
/dev/sda5  *         4096 209719295 209715200   100G 83 Linux
/dev/sda6       209721344 419436543 209715200   100G 83 Linux
/dev/sda7       419438592 429918207  10479616     5G 82 Linux swap / Solaris
```

test fio random (IOPS) and sequential (MBps)


