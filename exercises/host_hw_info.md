# Extract hardware info from host with linux

## system

### system model, bios version and bios date, how old is the hardware?

## mainboard model, link to manual, link to product

### memory banks (free or occupied)

### how many disks and types can be connected

### chipset, link to 

## cpu

### cpu model, year, cores, threads, cache 

### socket 

## pci

### number of pci slots, lanes available
 
### devices connected

### network device, model, kernel module, speed

### audio device, model, kernel module

### vga device, model, kernel module

## hard disks

### /dev/* , model, bus type, bus speed

### test fio random (IOPS) and sequential (MBps)

